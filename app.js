#!/usr/bin/env node
var blockchain = require('mastercard-blockchain');
var MasterCardAPI = blockchain.MasterCardAPI;

const async = require('async'), encoding = 'base64', fs = require('fs'), express = require('express'),
  bodyParser = require('body-parser'), request = require('request'), crypto = require('crypto');
var prompt = require('prompt'), options = createOptions();

var protobuf = require("protobufjs");

var argv = options.argv;
prompt.override = argv;

var moderatorRef = "abc123efg";
var appID = null;
var msgClassDef = null;
var protoFile = null;
var protoFileContent = null;
var timeout = 30000;
var sleeptime = 1000;

console.log(fs.readFileSync('help.txt').toString());
prompt.start();
initApi((err, result) => {
  if (!err) {
    updateNode((error, data) => {
      processCommands();
    });
  }
});

function processCommands() {
  async.waterfall([
    function (callback) {
      prompt.get(promptCmdSchema(), callback);
    }
  ], function (err, result) {
    if (!err) {
      switch (result.cmdOption) {
        case 1:
          updateNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 2:
          startModeratorNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 3:
          startSenderNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 4:
          createTransactionEntry(null, (error, data) => {
            processCommandsAfter();
          });
          break;
        case 5:
          retrieveTransactionEntry((error, data) => {
            processCommandsAfter();
          });
          break;
        case 6:
          options.showHelp();
          async.nextTick(processCommandsAfter);
          break;
        default:
          console.log('Goodbye');
          break;
      }
    }
  });
}

function updateNode(callback) {
  console.log('updateNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'Protofile',
        default: 'transaction.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        blockchain.App.update({
          id: appID,
          name: appID,
          description: "",
          version: 0,
          definition: {
            format: "proto3",
            encoding: encoding,
            messages: fs.readFileSync(protoFile).toString(encoding)
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    },
    function (result, callback) {
      blockchain.App.read(appID, {}, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function startModeratorNode(callback) {
  console.log('startModeratorNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'port',
        description: 'Listening port',
        default: '8080',
        required: true,
        conform: (value) => {
          return 0 < parseInt(value);
        }
      }, callback);
    }
  ], function (err, data) {
    if (err) {
      console.log('error', err);
      async.nextTick(callback, err, null);
    } else {
      var app = express();
      app.set('port', parseInt(data.port));
      app.use(bodyParser.json());
      app.post('/payment', function (req, resp) {
        req.body.moderatorRef = moderatorRef;
        var errMsg = msgClassDef.verify(req.body);
        if (errMsg) {
          resp.json({
            moderatorRef: '',
            vaid: false
          });
        } else {
          var hashToFind = hashData(hashData(msgClassDef.encode(msgClassDef.create(req.body)).finish())).toString('hex')
          resp.json({
            moderatorRef: moderatorRef,
            vaid: true
          });
          console.log('Beginning polling of blockchain for prospective transaction with hash ' + hashToFind);
          poolBlockchain(null, hashToFind, new Date().getTime() + timeout);
        }
      });
      var server = app.listen(app.get('port'), function () {
        console.log('(press return at any time to stop server)');
      });
      waitForEnter(() => {
        server.close();
        console.log('(stopped server)');
        async.nextTick(callback, err, {});
      });
    }
  });
}

function poolBlockchain(fromBlock, hashToFind, timeout) {
  var req = {};
  if (fromBlock) {
    req.from = fromBlock;
  }
  blockchain.Block.list(req, (err, resp) => {
    if (0 < resp.length) {
      async.concatSeries(resp, (block, callback) => {
        var res = { block: null, found: false };
        block.partitions.forEach(function (partition) {
          var posFound = partition.entries.find(function (element) {
            return hashToFind === element;
          }, this);
          if (posFound) {
            res.block = block;
            res.found = true;
          }
        }, this);
        callback(null, res);
      }, (err, results) => {
        if (err) {
          console.log('error', err);
        } else {
          var match = results.find((item) => { return item.found; });
          if (match) {
            console.log('Transaction found at slot', match.block.slot);
          } else if (new Date().getTime() > timeout) {
            console.log('Entry not found within time limit, exiting poll.');
          } else {
            console.log('Entry not found, retrying.');
            async.nextTick(() => {
              setTimeout(() => { poolBlockchain(resp[resp.length - 1].slot, hashToFind, timeout); }, sleeptime);
            });
          }
        }
      });
    } else {
      console.log('Empty block list');
    }
  });
}

function startSenderNode(callback) {
  console.log('startSenderNode');
  var ctx = {};
  async.waterfall([
    function (callback) {
      prompt.get(promptTransSendEntrySchema(), callback);
    },
    function (data, callback) {
      ctx.req = {
        from: data.from, to: data.to, amount: parseInt(data.amount)
      };
      request({
        uri: 'http://localhost:' + data.port + '/payment',
        method: 'POST',
        json: ctx.req
      }, callback);
    },
    function (resp, body, callback) {
      ctx.req.modRef = body.moderatorRef;
      createTransactionEntry(ctx.req, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    }
    async.nextTick(callback, err, result);
  });
}

function createTransactionEntry(payload, callback) {
  console.log('createTransactionEntry');
  async.waterfall([
    function (callback) {
      if (payload) {
        callback(null, payload);
      } else {
        prompt.get(promptTransEntrySchema(), callback);
      }
    },
    function (data, callback) {
      var payload = { from: data.from, to: data.to, amount: parseInt(data.amount), moderatorRef: data.modRef };
      var errMsg = msgClassDef.verify(payload);
      if (errMsg) {
        callback(errMsg, null);
      } else {
        var message = msgClassDef.create(payload);
        blockchain.TransactionEntry.create({
          "app": appID,
          "encoding": encoding,
          "value": msgClassDef.encode(message).finish().toString(encoding)
        }, callback);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function retrieveTransactionEntry(callback) {
  console.log('retrieveTransactionEntry');
  var ctx = {};
  async.waterfall([
    function (callback) {
      prompt.get(['hash'], callback);
    },
    function (data, callback) {
      blockchain.TransactionEntry.read("", { "hash": data.hash }, callback);
    },
    function (data, callback) {
      ctx.data = data;
      var message = msgClassDef.decode(new Buffer(data.value, 'hex'));
      var object = msgClassDef.toObject(message, {
        longs: String,
        enums: String,
        bytes: String
      });
      callback(null, object);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log('response', ctx.data, 'decoded', result);
    }
    async.nextTick(callback, err, ctx.data, result);
  });
}

function hashData(data) {
  var hash = crypto.createHash('sha256');
  hash.update(data);
  return hash.digest();
}

function initApi(onDone) {
  console.log('initializing');
  async.waterfall([
    function (callback) {
      prompt.get(promptInitSchema(), callback);
    },
    function (result, callback) {
      var authentication = new MasterCardAPI.OAuth(result.consumerKey, result.keystorePath, result.keyAlias, result.storePass);
      MasterCardAPI.init({
        sandbox: true,
        debug: argv.verbosity,
        authentication: authentication
      });
      protoFile = result.protoFile;
      protobuf.load(protoFile, callback);
    }
  ], function (err, root) {
    if (err) {
      console.log('error', err);
    } else {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        console.log('initialized');
      } else {
        console.log('could not read message class def from', protoFile);
      }
    }
    async.nextTick(onDone, err, appID);
  });
}

function waitForEnter(callbackTo) {
  async.waterfall([
    function (callback) {
      prompt.get({ properties: { enter: { description: 'press enter to continue', required: false } } }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    }
    async.nextTick(callbackTo);
  });
}

function processCommandsAfter() {
  waitForEnter(processCommands);
}

function promptInitSchema() {
  return {
    properties: {
      keystorePath: {
        description: 'the path to your keystore (mastercard developers)',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      },
      storePass: {
        description: 'keystore password (mastercard developers)',
        required: true,
        default: 'keystorepassword'
      },
      consumerKey: {
        description: 'consumer key (mastercard developers)',
        required: true
      },
      keyAlias: {
        description: 'key alias (mastercard developers)',
        required: true,
        default: 'keyalias'
      },
      protoFile: {
        description: 'the path to the protobuf file',
        required: true,
        default: 'transaction.proto'
      }
    }
  };
}

function promptTransSendEntrySchema() {
  return {
    properties: {
      from: {
        description: 'from',
        required: true,
        default: 'iban:1234'
      },
      to: {
        description: 'to',
        required: true,
        default: 'iban:5678'
      },
      amount: {
        description: 'amount',
        required: true,
        default: '1234',
        conform: (value) => {
          return parseInt(value) > 0;
        }
      },
      port: {
        description: 'Target port',
        default: '8080',
        required: true,
        conform: (value) => {
          return 0 < parseInt(value);
        }
      }
    }
  };
}

function promptTransEntrySchema() {
  return {
    properties: {
      from: {
        description: 'from',
        required: true,
        default: 'iban:1234'
      },
      to: {
        description: 'to',
        required: true,
        default: 'iban:5678'
      },
      amount: {
        description: 'amount',
        required: true,
        default: '1234',
        conform: (value) => {
          return parseInt(value) > 0;
        }
      },
      modRef: {
        description: 'moderator reference',
        required: true,
        default: 'abc123'
      }
    }
  };
}

function promptCmdSchema() {
  return {
    properties: {
      cmdOption: {
        description: '\n============ MENU ============\n1. Update node\n2. Mimic Moderator Node\n3. Mimic Send Node\n4. Create transaction entry\n5. Retrieve transaction entry\n6. Print Command Line Options\n0. Quit\nOption',
        message: 'Invalid Option',
        type: 'number',
        default: 0,
        required: true,
        conform: (value) => {
          return value >= 0 && value <= 6;
        }
      }
    }
  };
}

function createOptions() {
  return require('yargs')
    .options({
      'consumerKey': {
        alias: 'ck',
        description: 'consumer key (mastercard developers)'
      },
      'keystorePath': {
        alias: 'kp',
        description: 'the path to your keystore (mastercard developers)'
      },
      'keyAlias': {
        alias: 'ka',
        description: 'key alias (mastercard developers)'
      },
      'storePass': {
        alias: 'sp',
        description: 'keystore password (mastercard developers)'
      },
      'protoFile': {
        alias: 'pf',
        description: 'protobuf file'
      },
      'verbosity': {
        alias: 'v',
        default: false,
        description: 'log mastercard developers sdk to console'
      }
    });
}

function getProperties(obj) {
  var ret = [];
  for (var name in obj) {
    if (obj.hasOwnProperty(name)) {
      ret.push(name);
    }
  }
  return ret;
}

function guessNested(root) {
  var props = getProperties(root.nested);
  var firstChild = getProperties(root.nested[props[0]].nested);
  return [props[0], firstChild[0]];
}
